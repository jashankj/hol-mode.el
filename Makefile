hol-mode.el: hol-mode.src
	sed \
		-e 's@HOL-EXECUTABLE@"hol"@' \
		-e 's@HOLMAKE-EXECUTABLE@"Holmake"@' \
		< hol-mode.src \
		> hol-mode.el
