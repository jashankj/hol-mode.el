[ -e HOL ] || \
git clone --reference ../HOL --dissociate https://github.com/HOL-Theorem-Prover/HOL.git
cd HOL

env FILTER_BRANCH_SQUELCH_WARNING=1 \
git filter-branch -f --prune-empty \
	--index-filter "\
	git ls-tree -r \$GIT_COMMIT \
	| perl $PWD/../GENERATE0.pl \
	| git update-index --index-info
	" \
	-- \
	--all

# git rm -r -q --cached --ignore-unmatch -- Holmake/

cp ../GENERATE.sh .
cp ../COPYRIGHT   .
cp ../Makefile    .
cp ../README.md   .
echo "hol-mode.el" > .gitignore
git add GENERATE.sh
git add COPYRIGHT
git add Makefile
git add README.md
git add .gitignore
