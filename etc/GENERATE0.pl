#!/usr/bin/env perl

use strict;
use warnings;
use feature 'say';
use feature 'switch';
no warnings 'experimental::smartmatch';
use feature 'signatures';
no warnings 'experimental::signatures';

sub iput($mode, $oid, $name) {
	say sprintf "\%s \%40s\t\%s", $mode, $oid, $name;
}
sub irm($f) { iput 0, 0 x 40, $f }

while (<>) {
	chomp;
	my ($mode, $blob, $oid, $name) = split /\s/, $_, 4;

	given ($name) {
		when (
			# Pick up the mode and any helpers.
			m{^tools/.*-mode\.src$} or
			m{^tools/.*\.el$} or
			# (Also, pick up the old name, to preserve history.)
			m{^tools/.*\.emacs$} or
			# Pick up the ML helper script too.
			m{^tools/.*-mode\.sml$} or
			# Pick up yasnippets.
			m{^tools/yasnippets/} or
			# Pick up the test suite.
			m{^tools/mode-tests/} or
			# Pick up the documentation.
			m{^doc/[^-]+-mode} or
			m{^COPYRIGHT$}
		) {
			# Remove the original filename.
			irm $name;
			# Generate the new name.
			$name =~ s#^tools/##;
			$name =~ s#^doc/[^-]+-mode/#doc/#;
			# Recreate the file with the new name.
			iput $mode, $oid, $name;
		}
		default { irm $name; }
	}
}
